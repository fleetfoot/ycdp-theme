export const headerInit = (e) => {
  let scrollpos = window.scrollY
  const header = document.getElementById('ycdp-header')
  const headerHeight = header.offsetHeight

  const addClassOnScroll = () => header.classList.add('fade-in')
  const removeClassOnScroll = () => header.classList.remove('fade-in')

  window.addEventListener('scroll', function () {
    scrollpos = window.scrollY

    if (scrollpos >= headerHeight && e.matches) {
      addClassOnScroll()
    } else {
      removeClassOnScroll()
    }
  })
}
