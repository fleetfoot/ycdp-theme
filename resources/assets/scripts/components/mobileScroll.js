export const mobileScroll = (e) => {
  if (e.matches) {
    const menuItems = document.querySelectorAll(
      '.menu-primary-navigation-container .menu-item'
    )

    menuItems.forEach((item) => {
      item.addEventListener('click', () => {
        document.documentElement.classList.remove('sidebar-is-active')
        document.getElementById('hamburger').classList.remove('is-active')
      })
    })
  }
}
