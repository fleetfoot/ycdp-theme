import { headerInit } from '../components/header.js'
import { usingMouseInit } from '../components/usingMouse.js'
import { mobileScroll } from '../components/mobileScroll.js'
import Rellax from 'rellax'

export default {
  init() {
    $('.hamburger').click(function () {
      $(this).toggleClass('is-active')
      $('html').toggleClass('sidebar-is-active')
    })

    const mediaQuery = window.matchMedia('(min-width: 1160px)')
    const maxMediaQuery = window.matchMedia('(max-width: 1159px)')
    mediaQuery.addListener(headerInit)
    headerInit(mediaQuery)
    mobileScroll(maxMediaQuery)
    usingMouseInit()

    if (mediaQuery.matches) {
      new Rellax('.rellax')
    }
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
}
