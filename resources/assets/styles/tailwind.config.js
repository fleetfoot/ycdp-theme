/*
|-------------------------------------------------------------------------------
| Tailwind – The Utility-First CSS Framework
|-------------------------------------------------------------------------------
|
| Documentation at https://tailwindcss.com
|
*/

/**
 * Global Styles Plugin
 *
 * This plugin modifies Tailwind’s base styles using values from the theme.
 * https://tailwindcss.com/docs/adding-base-styles#using-a-plugin
 */
const { PWD } = process.env

const globalStyles = ({ addBase, config }) => {
  addBase({
    a: {
      color: config('theme.textColor.primary'),
      textDecoration: 'none',
      transition: '0.2s ease',
    },
    'a:hover': {
      color: config('theme.textColor.primaryDark'),
    },
    'ol, ul': { paddingLeft: config('theme.padding.4') },
    ol: { listStyleType: 'decimal' },
    ul: { listStyleType: 'disc' },
  })
}

/**
 * Configuration
 */
module.exports = {
  theme: {
    colors: {
      primary: '#5cc5c6',
      primaryDark: '#019a9b',
      white: '#fff',
      lightGray: '#f5f5f5',
      copyrightGray: '#8d8d8d',
      borderGray: '#dddddd',
      gray: '#333333',
      transparent: 'transparent',
    },
    // shadows: {
    //   outline: '0 0 0 3px rgba(82,93,220,0.3)',
    // },
    container: {
      center: true,
      padding: '1rem',
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xlg: '1160px',
      xl: '1340px',
      x2l: '1650px',
    },
    fontFamily: {
      sans: ['Lato', 'sans-serif'],
    },
    height: {
      auto: 'auto',
      full: '100%',
      sm: '50px',
      284: '284px',
      md: '382px',
      xl: '850px',
    },
    letterSpacing: {
      wide: '1.85px',
    },
    extend: {
      borderRadius: {
        5: '5px',
      },
      spacing: {
        p4: '4px',
        12: '12px',
        14: '14px',
        sm: '16px',
        20: '20px',
        md: '25px',
        30: '30px',
        lg: '40px',
        xl: '50px',
        60: '60px',
        67: '67px',
        80: '80px',
        106: '106px',
        xxl: '110px',
      },
      fontSize: {
        10: '10px',
        sm: '12px',
        14: '14px',
        16: '16px',
        17: '17px',
        18: '18px',
        lg: '20px',
        h1: '48px',
        h2: '42px',
        h3: '30px',
      },
      maxWidth: {
        460: '460px',
      },
    },
  },
  variants: {
    // Define variants
  },
  plugins: [globalStyles],
  corePlugins: {
    float: false,
  },
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: {
    content: [
      `${PWD}/resources/views/**/*.blade.php`,
      `${PWD}/resources/assets/scripts/**/*.js`,
    ],
    options: {
      whitelist: ['lg:pr-106', 'lg:pl-106', 'lg:flex-row-reverse'],
    },
  },
  darkMode: false,
}
