<?php
    $button_text = block_value('button-text');
    $button_url = block_value('button-url');

    $image_id = block_value('background-image');
    $image_url = wp_get_attachment_image_src($image_id);
?>

<header class="hub-section hub-section--bg-image" style="background-image: url(<?= esc_url($image_url[0]) ?>)">
    <div class="hub-header hub-container">
        <h1><?= get_the_title() ?></h1>
        <p><?php block_field('subtitle') ?></p>
        <a href="<?= esc_url($button_url) ?>"><?= esc_html($button_text)?></a>
    </div>
</header>
