<section class="ycdp-container ycdp-section relative flex flex-col lg:flex-row">
  <div class="lg:w-1/2 lg:pr-lg">
    <div class="module flex flex-col lg:flex-row pb-30 lg:pb-lg">
      <h4 class="uppercase text-primary text-sm tracking-wide font-bold w-67 flex flex-col items-center mb-3 lg:mb-2 lg:mt-1">{{ __('Module') }}
        <span class="hidden lg:inline-block text-h1 font-light mt-2">1</span>
      </h4>

      <div class="lg:pl-lg">
        <p class="font-bold">{{ $ycdp_module_1->title }}</p> 
        <p class="mb-0 border-b border-borderGray pb-30 lg:pb-lg">{{ $ycdp_module_1->text }}</p> 
      </div>
    </div>

    <div class="module flex flex-col lg:flex-row pb-30 lg:pb-lg">
      <h4 class="uppercase text-primary text-sm tracking-wide font-bold w-67 flex flex-col items-center mb-3 lg:mb-2 lg:mt-1">{{ __('Module') }}
        <span class="hidden lg:inline-block text-h1 font-light mt-2">2</span>
      </h4>

      <div class="lg:pl-lg">
        <p class="font-bold">{{ $ycdp_module_2->title }}</p> 
        <p class="mb-0 border-b border-borderGray pb-30 lg:pb-lg">{{ $ycdp_module_2->text }}</p> 
      </div>
    </div>

    <div class="module flex flex-col lg:flex-row">
      <h4 class="uppercase text-primary text-sm tracking-wide font-bold w-67 flex flex-col items-center mb-3 lg:mb-2 lg:mt-1">{{ __('Module') }}
        <span class="hidden lg:inline-block text-h1 font-light mt-2">3</span>
      </h4>

      <div class="pb-30 lg:pl-lg">
        <p class="font-bold">{{ $ycdp_module_3->title }}</p> 
        <p class="mb-0 border-b border-borderGray lg:border-none pb-30 lg:pb-0">{{ $ycdp_module_3->text }}</p> 
      </div>
    </div>
  </div>

  <div class="lg:w-1/2">
    <div class="module flex flex-col lg:flex-row pb-30 lg:pb-lg">
      <h4 class="uppercase text-primary text-sm tracking-wide font-bold w-67 flex flex-col items-center mb-3 lg:mb-2 lg:mt-1">{{ __('Module') }}
        <span class="hidden lg:inline-block text-h1 font-light mt-2">4</span>
      </h4>

      <div class="lg:pl-lg">
        <p class="font-bold">{{ $ycdp_module_4->title }}</p> 
        <p class="mb-0 border-b border-borderGray pb-30 lg:pb-lg">{{ $ycdp_module_4->text }}</p> 
      </div>
    </div>

    <div class="module flex flex-col lg:flex-row lg:pb-lg">
      <h4 class="uppercase text-primary text-sm tracking-wide font-bold w-67 flex flex-col items-center mb-3 lg:mb-2 lg:mt-1">{{ __('Module') }}
        <span class="hidden lg:inline-block text-h1 font-light mt-2">5</span>
      </h4>

      <div class="lg:pl-lg">
        <p class="font-bold">{{ $ycdp_module_5->title }}</p> 
        <p class="mb-0">{{ $ycdp_module_5->text }}</p> 
      </div>
    </div>
  </div>

  <div class="flair module__flair">
    <div class="flair__bg module__flair__bg rellax"
      data-rellax-speed="1"
      style="background-image: url(@asset('images/flair-2.png'))">
    </div>
  </div>
</section>
