<section id="downloads" class="ycdp-container ycdp-section relative downloads">
  <h3 class="text-center mb-20 xlg:mb-xl">{{ __('Downloads') }}</h3>

  <div class="flex flex-col xlg:flex-row items-center xlg:justify-between space-y-20 xlg:space-y-0 xlg:space-x-lg">
    @component('components.download', [
      'file' => $ycdp_download_1->file,
      'text' => $ycdp_download_1->text
    ])
    @endcomponent
      
    @component('components.download', [
      'file' => $ycdp_download_2->file,
      'text' => $ycdp_download_2->text
    ])
    @endcomponent

    @component('components.download', [
      'file' => $ycdp_download_3->file,
      'text' => $ycdp_download_3->text
    ])
    @endcomponent
  </div>

  <div class="flair downloads__flair">
    <div class="flair__bg downloads__flair__bg rellax"
      data-rellax-speed="-1"
      style="background-image: url(@asset('images/flair-3.png'))"></div>
  </div>
</section>
