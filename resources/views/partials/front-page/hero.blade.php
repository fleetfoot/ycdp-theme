@php
$url = $ycdp_hero_link->url;
$text = $ycdp_hero_link->title;
$target = $ycdp_hero_link->target ? $ycdp_hero_link->target : '_self';
@endphp

<header class="relative hero mx-auto overflow-hidden">
  <div class="hero__container ycdp-container pb-80 sm:pb-0 lg:h-full flex flex-col items-start justify-center hero__content">
    <div class="lg:w-1/2 lg:pt-10">
      <h1 class="hero__title">{{ get_the_title() }}</h1>

      <div class="hero__text">{!!  wp_kses_post($ycdp_hero_text) !!}</div>

      <a class="btn text-lg py-sm px-8 lg:px-10 mb-0 font-bold inline-block" href="{{ $url }}" target="{{ $target }}">{{ $text }}</a>
    </div>
  </div>

  <div class="hero__bg-color"></div>

  <img class="hero__bg-image rellax"
    data-rellax-speed="-5"
    srcset="{{ $ycdp_hero_bgm->url }} 640w, {{ $ycdp_hero_bg->url }} 768w"
    src="{{ $ycdp_hero_bg->url }}"
    sizes="(max-width: 640px) 640px, 768px"
    alt="Background Image"
  />
</header>
