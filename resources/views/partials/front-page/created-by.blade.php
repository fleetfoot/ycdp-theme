<section class="created-by ycdp-container">
    <h3 class="text-center mb-lg lg:mb-xl">{{$ycdp_cb_title}}</h3>

    <div class="flex flex-col lg:flex-row items-center lg:items-start lg:justify-around">
      @component('components.centered-text-and-logo', [
        'content' => $ycdp_cb_left
        ])
      @endcomponent

      @component('components.centered-text-and-logo', [
        'content' => $ycdp_cb_right
        ])
      @endcomponent
    </div>
</section>
