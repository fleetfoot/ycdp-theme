@php
$target = $ycdp_cta_link->target ? $ycdp_cta_link->target : '_self';
@endphp

<section class="cta relative mx-auto h-284 lg:h-md bg-cover mt-xl lg:mt-xxl">
  <div class="max-w-screen-md mx-auto flex flex-col justify-center items-center w-full h-full">
    <h4 class="text-white text-h3 lg:text-h2 font-bold text-center">{{ $ycdp_cta_title  }}</h4>
    <a class="btn text-lg font-bold py-sm px-10 mb-0" href="{{ $ycdp_cta_link->url }}" target="{{ $target }}">{{ $ycdp_cta_link->title }}</a>
  </div>

  <img
    loading="lazy"
    class="cta__bg-image"
    src="{{ $ycdp_cta_bg->url }}"
    alt="{{ $ycdp_cta_bg->alt }}"
    data-rellax-speed="1"
  />
</section>
