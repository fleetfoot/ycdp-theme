@php
$year = get_the_date( 'Y' );
@endphp

<footer class="footer content-info bg-lightGray relative">
  <div class="ycdp-container flex flex-col items-center pt-30 pb-20 lg:py-xl">
    @php dynamic_sidebar('sidebar-footer') @endphp

    <p class="text-10 lg:text-sm text-copyrightGray">{{ __('© Springboard')  }} {{ $year }}{{ __('. All rights reserved.') }}</p>
  </div>

  <div class="flair cta__flair">
    <div class="flair__bg cta__flair__bg"
      style="background-image: url(@asset('images/flair-4.png'))">
    </div>
  </div>
</footer>
