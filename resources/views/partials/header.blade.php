<header id="ycdp-header" class="banner ycdp-header overflow-hidden">
  <div class="ycdp-container py-md flex items-center justify-between">
    <div class="ycdp-header__logo ycdp-logo relative">

      @if ('fr' === pll_current_language('slug'))
        <img class="ycdp-header__logo ycdp-logo--light" src="@asset('images/logo-fr.svg')" alt="Youth Cannabis Diversion Program Logo" />
        <img class="ycdp-header__logo ycdp-logo--dark" src="@asset('images/logo-black-fr.svg')" alt="Youth Cannabis Diversion Program Logo" />
      @else
        <img class="ycdp-header__logo ycdp-logo--light" src="@asset('images/logo-en.svg')" alt="Youth Cannabis Diversion Program Logo" />
        <img class="ycdp-header__logo ycdp-logo--dark" src="@asset('images/logo-black-en.svg')" alt="Youth Cannabis Diversion Program Logo" />
      @endif
    </div>

    <button id="hamburger" class="xlg:invisible hamburger hamburger--spring" type="button"
            aria-label="Menu" aria-controls="ycdp-nav">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>

    @component('components.nav', [
      'id' => 'ycdp-nav-desktop',
      'class' => 'ycdp-nav--desktop desktop-only',
      'dropdown_id' => '2'
    ])
    @endcomponent
  </div>
</header>

