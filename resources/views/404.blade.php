@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  <div class="ycdp-container alert alert-warning">
    <p>{{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}</p>

    <a class="btn inline-block text-lg mt-lg font-bold" href="{{ get_home_url() }}">{{ __('Home Page') }}</a>
  </div>
@endsection
