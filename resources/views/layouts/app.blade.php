<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @component('components.nav', [
      'id' => 'ycdp-nav-mobile',
      'class' => 'ycdp-nav--mobile mobile-only',
      'dropdown_id' => 1,
    ])
    @endcomponent

    <div class="wrap" role="document">
      @php do_action('get_header') @endphp
      @include('partials.header')
      <div class="content">
        <main class="main space-y-lg lg:space-y-xxl">
          @yield('content')
        </main>
      </div>
      @php do_action('get_footer') @endphp
      @include('partials.footer')
    </div>
    @php wp_footer() @endphp
  </body>
</html>
