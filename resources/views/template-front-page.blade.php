{{--
  Template Name: Front Page
--}}

@extends('layouts.app')

@section('content')
  @include('partials.front-page.hero')

  @component('components.text-image-row', [
    'direction' => 'flex-row',
    'mobile_direction' => 'flex-col-reverse',
    'dark_bg' => false,
    'id' => 'about',
    'img' => $ycdp_ti_image,
    'title' => $ycdp_ti_title,
    'text' => $ycdp_ti_text
    ])
  @endcomponent

  @include('partials.front-page.modules')

  @component('components.text-image-row', [
    'direction' => 'flex-row-reverse',
    'mobile_direction' => 'flex-col',
    'dark_bg' => true,
    'id' => 'refer',
    'img' => $ycdp_ti2_image,
    'title' => $ycdp_ti2_title,
    'text' => $ycdp_ti2_text
    ])
  @endcomponent

  @include('partials.front-page.downloads') 
  @include('partials.border')
  @include('partials.front-page.created-by') 
  @include('partials.cta-bg-image') 
@endsection
