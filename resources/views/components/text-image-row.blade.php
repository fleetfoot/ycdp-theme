@php
if ('flex-row-reverse' === $direction) :
  $padding = "pr-106";
else :
  $padding = "pl-106";
endif;

if ('flex-col' === $mobile_direction) :
  $p = "mb-lg lg:mb-0";
else :
  $p = "mt-lg lg:mt-0";
endif;

if ($dark_bg) :
  $bg = 'bg-lightGray pt-60 pb-xl lg:py-xxl';
else:
  $bg = 'bg-white';
endif;
@endphp

<section id="{{$id}}" class="text-image-row relative ycdp-section {{ $bg }}">
  <div class="ycdp-container flex {{ $mobile_direction }} lg:{{ $direction }}">
    <img loading="lazy" class="lg:w-1/2 h-full object-contain {{ $p }}" src="{{ $img->url }}" alt="{{ $img->alt }}" />

    <div class="flex flex-col justify-center lg:mb-0 lg:w-1/2 lg:{{ $padding }}">
      <h3 class="font-bold mb-14 lg:mb-4">{{ $title }}</h3>
      <div class="text-image-row__text">{!! wp_kses_post($text) !!}</div>
    </div>
  </div>

  @if ('about' === $id)
    <div class="flair hero__flair">
      <div class="flair__bg hero__flair__bg rellax"
        data-rellax-speed="1"
        style="background-image: url(@asset('images/flair-1.png'))">
      </div>
    </div>
  @endif
</section>
