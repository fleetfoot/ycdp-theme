<nav id="{{ $id }}" class="ycdp-nav {{ $class }}">
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
  @endif

  <div class="select">
    @php 
    pll_the_languages( [
        'dropdown' => $dropdown_id,
        'display_names_as' => 'slug'
      ] );
    @endphp
  </div>
</nav>
