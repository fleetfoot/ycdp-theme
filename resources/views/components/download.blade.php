<a class="downloads__item link-gray border-primary hover:border-primaryDark border rounded-5 px-20 xlg:px-30 py-sm xlg:py-20 xlg:w-1/3" href="{{ $file }}">
  <div class="flex w-full items-center">
    <img src="@asset('images/icon-pdf.svg')" alt="PDF" />

    <p class="mb-0 px-3 flex-grow">{{ $text }}</p>

    <img src="@asset('images/icon-download.svg')" class="" alt="Download Button" />
  </div>
</a>
