@php
$logo_url = $content->logo->url;
$logo_alt = $content->logo->alt;
$text = $content->text;
$link_url = $content->link->url;
$link_text = $content->link->title;
$link_target = $content->link->target ? $content->link->target : '_self';
@endphp

<div class="centered-tl flex flex-col items-center justify-start max-w-460 mb-xl lg:mb-0">
  <img class="h-sm mb-20 lg:mb-30" src="{{$logo_url}}" alt="{{$logo_alt}}" />

  <div class="text-center mb-12">
    {!! wp_kses_post($text) !!}
  </div>

  <a class="inline-block w-auto link--arrow" href="{{$link_url}}" target="{{ $link_target }}">{{$link_text}}</a>
</div>
